
-- functionality with recon blips:
-- https://github.com/FAForever/fa/blob/deploy/fafdevelop/engine/Sim/ReconBlip.lua

-- functionality with units and entities:
-- https://github.com/FAForever/fa/blob/deploy/fafdevelop/engine/Sim/Unit.lua
-- https://github.com/FAForever/fa/blob/deploy/fafdevelop/engine/Sim/Entity.lua

-- a blip is also an entity, the source of a blip is typically a unit

local IntelTriggers = import("/maps/mellow_shallows_survival/managers/IntelTriggers.lua")

local debug = true
local cache = { }

--- Allows us to scan the cache for blips that are no longer valid
local function DecayThread()

    while true do 

        -- always wait a tad between iterations to prevent lag
        WaitSeconds(1.0)

        -- for each type, tech and blip...
        for j, type in cache do 
            for i, tech in type do 

                -- filter the blips that represent units that are alive
                local techAdj = { }
                for p, blip in tech do 
                    local unit = blip:GetSource()
                    if (not IsDestroyed(unit)) and (not unit.Dead) then 
                        table.insert(techAdj, blip)
                    end 
                end

                -- replace it with the blips that matter
                type[i] = techAdj
            end

            -- always wait a tad between types to prevent lag
            WaitSeconds(2.0)
        end
    end
end

--- Allows us to see what intel the AI has at any given moment
local function DebugThread()

    -- colors used to identify various intel, color keys match those
    -- used in the intel cache defined at the top of this file
    -- https://htmlcolorcodes.com/

    colors = { }
    colors.Power = "F7FC5F"
    colors.Mass = "008718"
    colors.Land = "770087"
    colors.Air = "84F2FF"
    colors.Naval = "000799"

    while true do 
        -- go over each category, tech and blip
        for j, type in cache do 
            for i, tech in type do 
                for p, blip in tech do 
                    if not IsDestroyed(blip) then 
                        -- determine center of circle
                        local position = blip:GetPosition()

                        -- determine radius of circle
                        local bp = blip:GetBlueprint()
                        local r = 0.75 * math.max(bp.SizeX, bp.SizeZ)

                        -- draw the circle
                        DrawCircle(position, r, colors[j])
                    end
                end
            end 
        end

        WaitSeconds(0.1)
    end
end 

--- Adjusts the on intel trigger to make it cheaper
local function AdjustBrainIntelTrigger(aiBrain)

    -- savor the old one
    local oldOnIntelChange = aiBrain.OnIntelChange

    -- construct our own
    local function OnIntelChange(self, blip, reconType, val)

        -- changes to the original:
        -- we don't check the owner of the blip
        -- we early-exit when we find one valid trigger

        -- if the blip enters intel...
        if val then 

            -- if the blip is in light of sight...
            if reconType == "LOSNow" then 
                for k, spec in self.IntelTriggerList do

                    -- if it meets our criteria...
                    local bp = blip:GetBlueprint()
                    if EntityCategoryContains(spec.Category, bp.BlueprintId) then
                        -- then do the callback
                        spec.CallbackFunction(blip)

                        -- we're not interested in other triggers
                        return
                    end
                end
            end
        end

        -- call the old on intel change
        oldOnIntelChange(self, blip, reconType, val)
    end

    -- override it with our own
    aiBrain.OnIntelChange = OnIntelChange
end

--- Initialises the intel gathering for the ai brain
function Initialise(aiBrain, brains)

    -- adjust the brain to speed it up slightly --

    AdjustBrainIntelTrigger(aiBrain)

    -- initialise the cache --

    -- add key categories
    cache.Power     = { }
    cache.Mass      = { }
    cache.Land      = { }
    cache.Air       = { }
    cache.Naval     = { }

    -- add tech sub categories
    for k, type in cache do 
        table.insert(type, { })
        table.insert(type, { })
        table.insert(type, { })
        table.insert(type, { })
    end 

    -- initialise trigger callbacks --

    --- Find the right tech in the cache
    local function InsertIntoType(type, unit, blip)

        local id = 1 
        if EntityCategoryContains(categories.TECH2, unit) then id = 2 end
        if EntityCategoryContains(categories.TECH3, unit) then id = 3 end
        if EntityCategoryContains(categories.EXPERIMENTAL, unit) then id = 4 end

        table.insert(type[id], blip)
    end 

    -- Determine the correct cache for economical blips
    local function EconomicCallback(blip)

        LOG("ECONOMIC BLIP")

        -- check if we've seen this unit before
        local unit = blip:GetSource()
        if not (unit.Seen) then 
            unit.Seen = true 

            if EntityCategoryContains(categories.ENERGYPRODUCTION, unit) then
                InsertIntoType(cache.Power, unit, blip)
            elseif EntityCategoryContains(categories.MASSPRODUCTION, unit) then
                InsertIntoType(cache.Mass, unit, blip)
            else 
                local bp = unit:GetBlueprint()
                WARN("Unknown economic blip: " .. repr(bp.BlueprintId))
            end
        end
    end 

    -- Determine the correct cache for factory blips
    local function FactoryCallback(blip)

        LOG("FACTORY BLIP")

        -- check if we've seen this unit before
        local unit = blip:GetSource()
        if not (unit.Seen) then 
            unit.Seen = true 

            if EntityCategoryContains(categories.LAND, unit) then
                InsertIntoType(cache.Land, unit, blip)
            elseif EntityCategoryContains(categories.AIR, unit) then
                InsertIntoType(cache.Air, unit, blip)
            elseif EntityCategoryContains(categories.NAVAL, unit) then
                InsertIntoType(cache.Naval, unit, blip)
            else 
                local bp = unit:GetBlueprint()
                WARN("Unknown factory blip: " .. repr(bp.BlueprintId))
            end
        end
    end

    -- attach triggers to brains --

    for k, brain in brains do 
        aiBrain:SetupArmyIntelTrigger(
            IntelTriggers.GetEconomicSpec(brain, EconomicCallback) )

        aiBrain:SetupArmyIntelTrigger(
            IntelTriggers.GetFactorySpec(brain, FactoryCallback) )
    end 

    -- initialise threading around cache --

    ForkThread(DecayThread)

    if ScenarioInfo.Debug and debug then 
        ForkThread(DebugThread)
    end 
end 

-- Picks a random element from the table
local function PickRandomElement(t)
    local n = table.getn(t)
    local i = math.floor(Random() * n) + 1
    return t[i]
end

--- Picks a blip from the highest tech available for the given type
function GetHighestBlip(key)

    -- retrieve the type with the key, check for consistency
    local type = cache[key]
    if not type then 
        WARN("Intel.GetHighestTarget: Key for type does not exist: " .. repr(key))
    end

    -- start at the highest tech to find a target
    for k = 4, 1, -1 do 
        -- if there are any targets, pick one randomly
        if table.getn(type[k]) > 0 then
            return PickRandomElement(type[k])
        end
    end 

    return nil
end

--- Picks a random blip for the given type
function GetRandomBlip(key)

    -- retrieve the type with the key, check for consistency
    local type = cache[key]
    if not type then 
        WARN("Intel.GetRandomTarget: Key for type does not exist: " .. repr(key))
    end

    -- find all tables that have one or more units
    local validTechs = { }
    for k, tech in type do 
        if table.getn(tech) > 0 then 
            table.insert(validTechs, tech)
        end
    end

    -- pick a random valid table and a random blip from that table
    local randomTech = PickRandomElement(validTechs)
    local randomBlip = PickRandomElement(randomTech)
    return randomBlip
    
end
