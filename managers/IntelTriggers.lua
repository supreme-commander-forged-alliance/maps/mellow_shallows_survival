
-- documentation from aiBrain.lua
-- INTEL TRIGGER SPEC
-- {
--    CallbackFunction = <function>,
--    Type = 'LOS'/'Radar'/'Sonar'/'Omni',
--    Blip = specific unit or false (any unit),
--    Value = true = when unit enters sight, false = when unit leaves sight
--    Category: blip category to match
--    OnceOnly: fire onceonly
--    TargetAIBrain: AI Brain of the army you want it to trigger off of.
-- },

-- Generic factory-like function for trigger specifications
local function GetTriggerSpec(targetBrain, callback, categories)
    local spec = { }
    spec.CallbackFunction = callback
    spec.Type = 'LOSNow'
    spec.Blip = false 
    spec.Value = true 
    spec.Category = categories
    spec.OnceOnly = false 
    spec.TargetAIBrain = targetBrain 
    return spec 
end 

-- A trigger specification for economic structures
function GetEconomicSpec(targetBrain, callback)
    return GetTriggerSpec(
        targetBrain, callback,
        categories.ENERGYPRODUCTION + categories.MASSPRODUCTION
    )
end 

-- A trigger specification for factory structures
function GetFactorySpec(targetBrain, callback)
    return GetTriggerSpec(
        targetBrain, callback,
        categories.FACTORY
    )
end 
