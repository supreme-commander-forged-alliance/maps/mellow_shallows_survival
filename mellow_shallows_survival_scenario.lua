version = 3 -- Lua Version. Dont touch this
ScenarioInfo = {
    name = "Mellow Shallows Survival",
    description = "A map inspired by oE_Empiran. Map made by (Jip) Willem Wijnia. You can find the latest version at: https://gitlab.com/supreme-commander-forged-alliance",
    preview = '',
    map_version = 2,
    type = 'skirmish',
    starts = true,
    size = {1024, 1024},
    reclaim = {30375.3, 119606.5},
    map = '/maps/mellow_shallows_survival/mellow_shallows_survival.scmap',
    save = '/maps/mellow_shallows_survival/mellow_shallows_survival_save.lua',
    script = '/maps/mellow_shallows_survival/mellow_shallows_survival_script.lua',
    norushradius = 0,
    Configurations = {
        ['standard'] = {
            teams = {
                {
                    name = 'FFA',
                    armies = {'ARMY_1', 'ARMY_2', 'ARMY_3', 'ARMY_4', 'ARMY_5', 'ARMY_6'}
                },
            },
            customprops = {
                ['ExtraArmies'] = STRING( 'ARMY_9 NEUTRAL_CIVILIAN ArmyOfGary' ),
            },
        },
    },
}
