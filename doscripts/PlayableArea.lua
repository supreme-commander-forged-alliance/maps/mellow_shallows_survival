
local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua')
local ScenarioFramework = import('/lua/ScenarioFramework.lua')

do 
    -- make all the players play inside our little box
    -- ScenarioFramework.SetPlayableArea('Area-1', false)

    -- make the AI unaware of this box
    SetIgnorePlayableRect("ArmyOfGary", true)
    
end
