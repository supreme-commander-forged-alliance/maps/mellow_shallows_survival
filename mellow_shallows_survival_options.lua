options = {
	{
		default 	= 2,
		label 		= "Startup time",
		help 		= "Defines the total startup time before shit hits the fan.",
		key 		= 'MellowShallowsStartupTime',
		pref	 	= 'MellowShallowsStartupTime',
		values 		= {
			{ text 	= "Easy", help = "All things considered, this is almost an holiday.", key = 300, },		
			{ text 	= "Normal", help = "The mission is as expected - there are no major hickups.", key = 240, },	
			{ text 	= "Heroic", help = "It's another day in the storm. Things are tough.", key = 200, },	
			{ text 	= "Legendary", help = "We haven't found a way to make it viable yet.", key = 180, },
		},	
	},

	{
		default 	= 2,
		label 		= "Handicap for player named Jammer",
		help 		= "Defines the total startup time before shit hits the fan.",
		key 		= 'MellowShallowsHandicap',
		pref	 	= 'MellowShallowsHandicap',
		values 		= {
			{ text 	= "Easy", help = "All things considered, this is almost an holiday.", key = '0.95', },		
			{ text 	= "Normal", help = "The mission is as expected - there are no major hickups.", key = '0.90', },	
			{ text 	= "Heroic", help = "It's another day in the storm. Things are tough.", key = '0.85', },	
			{ text 	= "Legendary", help = "We haven't found a way to make it viable yet.", key = '0.80', },
		},	
	},
}