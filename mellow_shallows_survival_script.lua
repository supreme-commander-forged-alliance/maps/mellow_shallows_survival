local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua')
local ScenarioFramework = import('/lua/ScenarioFramework.lua')
local Objectives = ScenarioFramework.Objectives

local Areas = import("/maps/mellow_shallows_survival/utilities/Areas.lua")
local Markers = import("/maps/mellow_shallows_survival/utilities/Markers.lua")
-- local Platoons = import("/maps/mellow_shallows_survival/utilities/Platoons.lua")
-- local NavalSpawns = import("/maps/mellow_shallows_survival/utilities/NavalSpawns.lua")
-- --local Drops = import("/maps/mellow_shallows_survival/generator/Drops.lua")
-- local NavalGenerator = import("/maps/mellow_shallows_survival/generator/NavalGenerator.lua")

local config = import("/maps/mellow_shallows_survival/config.lua").GetConfig()

local TriggerFile = import('/lua/scenariotriggers.lua')

local GenScouts = import("/maps/mellow_shallows_survival/generators/GenScouts.lua")

function OnPopulate()	

    ScenarioInfo.CampaignMode = true
    Sync.CampaignMode = true
    -- import('/lua/sim/simuistate.lua').IsCampaign(true)

	ScenarioUtils.InitializeArmies()
end

function OnStart(self)

	ScenarioInfo.Debug = true 

	doscript("/maps/mellow_shallows_survival/doscripts/PlayableArea.lua")
	doscript("/maps/mellow_shallows_survival/doscripts/Message.lua")
	doscript("/maps/mellow_shallows_survival/doscripts/Diplomacy.lua")

	ForkThread(
		function() 

			local ManagerIntel = import("/maps/mellow_shallows_survival/managers/Intel.lua")
			ManagerIntel.Initialise(
				GetArmyBrain("ArmyOfGary"),
				{ GetArmyBrain("ARMY_1") }
			)

			GenScouts.Generate(2)
		end
	)

	Markers.GetTransportAttackMarkers()
	Markers.GetTransportDropMarkers()

	Areas.GetSpawnNavyAreas()
	Areas.GetSpawnAirAreas()
	Areas.GetScoutAreas()	

	-- ScenarioFramework.CreateTimerTrigger(function() Drops.OnStart() end , config.startup, false)

	-- -- initialise everything
	-- Markers.OnStart()
	-- Platoons.OnStart()
	-- NavalSpawns.OnStart()
	-- NavalGenerator.OnStart()

    -- -- ScenarioInfo.M2B1Objective = Objectives.Protect(
    -- --     'bonus',
    -- --     'incomplete',
    -- --     OpStrings.M2B1Text,
    -- --     OpStrings.M2B1Detail,
    -- --     {
    -- --         Units = ScenarioInfo.M2City,
    -- --         nil,        if nil, requires a manual update for completion
    -- --         NumRequired = ScenarioInfo.TotalCityUnits,     How many must survive
    -- --     }
    -- -- )

	-- -- INFO: {
	-- -- 	INFO:   Active=true,
	-- -- 	INFO:   AddAreaTarget=function: 180E5168,
	-- -- 	INFO:   AddProgressCallback=function: 15D1C674,
	-- -- 	INFO:   AddResultCallback=function: 15D1C658,
	-- -- 	INFO:   AddUnitTarget=function: 180E5F28,
	-- -- 	INFO:   Complete=false,
	-- -- 	INFO:   Decal=false,
	-- -- 	INFO:   Decals={ },
	-- -- 	INFO:   Description="The start up time defined in the options.",
	-- -- 	INFO:   Fail=function: 15D1C6C8,
	-- -- 	INFO:   IconOverrides={ },
	-- -- 	INFO:   ManualResult=function: 180E5118,
	-- -- 	INFO:   NextTargetTag=0,
	-- -- 	INFO:   OnProgress=function: 15D1C6AC,
	-- -- 	INFO:   OnResult=function: 15D1C690,
	-- -- 	INFO:   PositionUpdateThreads={ },
	-- -- 	INFO:   ProgressCallbacks={ },
	-- -- 	INFO:   ResultCallbacks={ },
	-- -- 	INFO:   SimStartTime=0,
	-- -- 	INFO:   Tag="Objective0",
	-- -- 	INFO:   Title="Time before hell breaks lose",
	-- -- 	INFO:   UnitMarkers={ },
	-- -- 	INFO:   VizMarkers={ }
	-- -- 	INFO: }


	-- local brain = GetArmyBrain("ARMY_1")
	-- local units = brain:GetListOfUnits(categories.COMMAND, false, false)
	-- local commander = units[1]

	-- local count = commander:GetWeaponCount()
	-- local weapon1 = commander:GetWeapon(1)
	-- weapon1:ChangeRateOfFire(10)
	-- weapon1:SetFiringRandomness(10)

	-- local prioTable = { categories.FACTORY, categories.LAND, categories.ALLUNITS }
	-- weapon1:SetTargetingPriorities(prioTable)

	-- -- LOG(repr(categories))




	-- local name = "FactoryCount"
	-- local brain = GetArmyBrain("ARMY_1")
	-- local callback = function(brain)
	-- 	local units = brain:GetListOfUnits(categories.FACTORY, false, false)
	-- 	local unit = units[1]
	-- 	unit:Destroy()
	-- end
	-- local category = categories.FACTORY

	-- TriggerFile.CreateArmyStatTrigger( callback, brain, name,
	-- {
	-- 	{
	-- 		StatType = 'Units_BeingBuilt',
	-- 		CompareType = 'GreaterThanOrEqual', 
	-- 		Value = brain:GetBlueprintStat('Units_History', category) + 1, 
	-- 		Category = category,
	-- 	},
	-- }
	-- )

end
