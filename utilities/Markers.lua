
--- Picks a random position from the list of positions
function PickRandomPosition(positions)
    local n = table.getn(positions)
    local i = math.floor(Random() * n) + 1
    return positions[i]
end

--- Attempts to retrieve the markers, returning them as a table.
local function GetMarkersUncached(identifier)
    local t = { }

    -- see how many we can find
    local count = 1 
    local name = identifier .. count
    local markers = ScenarioInfo.Env.Scenario.MasterChain._MASTERCHAIN_.Markers

    while markers[name] do 

        -- it exists, process it
        table.insert(t, markers[name].position)

        -- check the next one
        count = count + 1 
        name = identifier .. count 
    end

    -- defensive checks for us developers
    if table.getn(t) == 0 then 
        error("Markers.GetMarkers: No markers found for " .. identifier)
    end

    -- debugging information
    if ScenarioInfo.Debug then 
        LOG("Utilities.Markers: Found " .. table.getn(t) .. " marker(s) for '" .. identifier .. "'.")
    end

    return t
end

-- keeps track of what we've found so far
local cache = { }

--- Attempts to retrieve the markers and caches them.
function GetMarkers(identifier)
    -- return cache if it exists
    if cache[identifier] then   
        return cache[identifier]
    end

    -- cache the spawn areas
    cache[identifier] = GetMarkersUncached(identifier)
    return cache[identifier]
end

--- Ease-of-access function for transport drop points.
function GetTransportDropMarkers()
    local identifier = "drop-transport-"
    return GetMarkers(identifier)
end

--- Ease-of-access function for transport attack points.
function GetTransportAttackMarkers()
    local identifier = "drop-attack-"
    return GetMarkers(identifier)
end