
--- Computes random point inside a random area
function PickRandomPoint(areas)
    local area = PickRandomElement(areas)
    return PickRandomPointInArea(area)
end

--- Picks a random area from the table
function PickRandomArea(areas)
    local n = table.getn(areas)
    local i = math.floor(Random() * n) + 1
    return areas[i]
end

--- Computes a random point inside an area (rectangle)
function PickRandomPointInArea(area)
    local diffx = area.x1 - area.x0
    local diffy = area.y1 - area.y0

    local rx = Random() * diffx
    local ry = Random() * diffy

    local x = area.x0 + rx 
    local z = area.y0 + ry
    local y = GetSurfaceHeight(x, z)

    return VECTOR3(x, y, z)
end

--- Attempts to retrieve the areas, returning them as a table.
local function GetAreasUncached(identifier)

    -- look for areas
    local t = { }

    -- see how many we can find
    local count = 1 
    local name = identifier .. count
    local areas = ScenarioInfo.Env.Scenario.Areas

    while areas[name] do 

        -- it exists, process it
        local area = areas[name]
        local rect = Rect(area.rectangle[1], area.rectangle[2], area.rectangle[3], area.rectangle[4])
        table.insert(t, rect)

        -- check the next one
        count = count + 1 
        name = identifier .. count 
    end

    -- defensive checks for us developers
    if table.getn(t) == 0 then  
        error("Areas.GetAreas: We found no areas with name " .. identifier)
    end

    -- debugging information
    if ScenarioInfo.Debug then 
        LOG("Utilities.Areas: Found " .. table.getn(t) .. " area(s) for '" .. identifier .. "'.")
    end

    return t 
end

-- keeps track of what we've found so far
local cache = { }

--- Attempts to retrieve the areas and caches them.
function GetAreas(identifier)
    -- return cache if it exists
    if cache[identifier] then   
        return cache[identifier]
    end

    -- cache the spawn areas
    cache[identifier] = GetAreasUncached(identifier)
    return cache[identifier]
end

--- Ease-of-access function for navy spawn areas.
function GetSpawnNavyAreas()
    local identifier = "spawn-navy-"
    return GetAreas(identifier)
end 

--- Ease-of-access function for air spawn areas.
function GetSpawnAirAreas()
    local identifier = "spawn-air-"
    return GetAreas(identifier)
end 

--- Ease-of-access function for scout areas.
function GetScoutAreas()
    local identifier = "scout-area-"
    return GetAreas(identifier)
end 