
local config = false 
function GetConfig() 

    -- return the cached config if available
    if config then 
        return config
    else 
        config = { }
    end 

    -- from global scope
    local options = ScenarioInfo.Options 
    local version = ScenarioInfo.map_version

    -- local scope
    config.version = version
    config.handicap = tonumber(options.MellowShallowsHandicap)
    config.startup = options.MellowShallowsStartupTime

    LOG(repr(config))

    return config 

end 