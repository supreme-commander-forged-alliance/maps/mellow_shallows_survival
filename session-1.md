### About 'dem transports!

Throughout this session we focussed on the core aspects of programming in Supreme Commander. We discussed:
 - What our Interactive Development Environment (IDE) is: [Visual Studio Code (VSC)](https://code.visualstudio.com/) for this particular series.
 - A workspace that we can query with questions: an instance of VSC that has the [FAF repository](https://github.com/FAForever/fa) and the original lua game files.
 - How functionality can be looked up in our workspace - such as where to find [functions}(https://github.com/FAForever/fa/tree/deploy/fafdevelop/engine) that are available to us globally.
 - How various files interact and why the import function is not magic.
 - How you can debug your code using `LOG(...)` and `repr(...)`.
 - How you can prepare your coding session by thinking ahead before actually programming it.

We concluded the session by applying these techniques to:
 - Preparing various platoons / waves in the editor, instead of in a script.
 - Preparing various spawn / move / attack markers in the editor.
 - Accessing the marker / wave information and storing the essentially bits in our scripts.
 - Spawning a single wave, attach it to a transport, move it via our markers and make them drop.

### Exercises

 - [ ] Add in various transport groups in the editor
 - [ ] Add in various navy groups in the editor
 - [ ] Spawn individual navy units via script
 - [ ] Spawn platoon navy units via script
 - [ ] Refactor the areas: make them have their own file (from Markers.lua -> Areas.lua)
 - [ ] Add in various nice functionality to that new file, such as:
 - - [ ] picking a random area
 - - [ ] picking a random point inside an area
