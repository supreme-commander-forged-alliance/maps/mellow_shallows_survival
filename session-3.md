### Refactoring: dependencies
 - Remove the weird dependencies between function calls
 - Add local caching

### Debugging
 - Explain about switching armies in the cheat window
 - Defensive programming: add error logging

### Options
 - Explain general format of options (keys can only be int / string)
 - Explain how to get fractals (with tonumber())
 - Explain about the config file

### Objectives
 - Explain about a few basic objectives, such as keep these units safe and destroy those
 - Explain about callbacks

### Attack priorities
 - Explain what categories are
 - Explain what these are (attack priorities mod)
 - Rough version of attack priorities

### Brain triggering
 - About intel triggers
 - About stat triggers
