### Wait, that is already implemented?

Throughout this session we focus on learning to debug, talk about threads and we'll be generating (safe) paths for our drops.
 - About debugging in general - writing your own debug code and how to use the moholog.
 - About threads in the context of Supreme Commander.
 - About army and unit callbacks and how they relate to threads.
 - About generating safe paths.
 - About computing the closest (safe) drop / spawn points.
 - About Refactoring

### Exercises

### Debugging
 - [ ] When you use LOG(repr(...)) on a recursive table the function loops indefinitely. Write your own stringifier that shows the keys, types and values for non-table elements without going into recursion.

### Threads
 - [ ] Create a basic thread that sends out a drop ship at a random spawn to a random drop every five seconds.
 - [ ] Create a more sophisticated thread that sends out a drop ship that attempts to evade threats given a desired drop point.

### Army / unit triggers
 - [ ] Create an army intel trigger that spawns a wave when a paragon is detected. (make sure that the army in question can gain vision over the paragon). You can find examples in the campaign files.
 - [ ] Create an army stat trigger that spawns a wave once when a player has more than 20 mass income. You can find examples in the campaign files.
 - [ ] Create a group death trigger for a platoon that when it dies a message is printed to the console. We'll use this in the next session.
 - [ ] Create a timer trigger that spawns a drop once it is finished.
 - [ ] Create a unit damaged and death trigger such that if a unit from a platoon is sufficiently damaged (say 50% or more) or destroyed the entire platoon triggers and engages the attacker.
 - - This behavior may seem rational - but when is it not? And can you prevent it from happening?

### Ai Attack Utilities
 - [ ] Use the `CheckNavalPathing` function to find a path for a naval platoon to one of the outer mass extractors.
 - [ ] Use the `PlatoonGenerateSafePathTo` function to find a safe path for UEF / Seraphim cruisers that are not part of a platoon to a target, such as a mass extractor.
 - [ ] Place oppertunity markers (blank markers that you expect to have oppertunities, such as extractors) in the editor - use these in combination with `aiBrain:GetUnitsAroundPoint` and `aiBrain:GetThreatsAroundPosition` to check whether there are mass extractors and whether these are defended or not. If not, send a drop.

### Further reading
 - [About pathing markers](https://forum.faforever.com/topic/343/about-markers-movement-pathing-markers)
 - [About A*](https://www.redblobgames.com/pathfinding/a-star/introduction.html)