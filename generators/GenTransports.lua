
local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua')
local ScenarioFramework = import('/lua/ScenarioFramework.lua')

local Markers = import("/maps/mellow_shallows_survival/utilities/Markers.lua")
local Platoons = import("/maps/mellow_shallows_survival/utilities/Platoons.lua")

local AIAttackUtils = import('/lua/ai/aiattackutilities.lua')

-- todo: make this an actual loop
function OnStart()

    local maximum = 5
    local thread = ForkThread(KeepThemDroppingThread, maximum)

end

function KeepThemDroppingThread(maximum)

    local count = 0

    while true do 

        WaitSeconds(5)

        count = count + 1 
        if count > maximum then
            break
        end

        local function OnDetachedThread(units, callback)
            -- keep running until no longer attached
            -- TODO : Fix the bug when all units are dead before dropping
            local attached = true;
            while attached do
                WaitSeconds(1)
                attached = false
                for k, unit in units do
                    if not unit.Dead then
                        if unit:IsUnitState('Attached') then
                            -- keep checking until they are _all_ free.
                            attached = true
                            break
                        end
                    end                
                end
            end
    
            -- do a callback
            callback(units)
        end
    
        -- determine all the locations
        local spawn = Markers.PickRandomPosition(Markers.spawnTransportMarkers)
        local drop = Markers.PickRandomPosition(Markers.dropTransportMarkers)
        local attack = Markers.PickRandomPosition(Markers.dropAttackMarkers)
    
        -- spawn the units and attach them
        local army = "ArmyOfGary"
        local group = Platoons.RandomArmyGroup()
        local transports, transportees = Platoons.SpawnArmyGroup(group, army, spawn)
        ScenarioFramework.AttachUnitsToTransports(transportees, transports)
    
        local aiBrain = GetArmyBrain(army)
        local layer = 'Air'
        local path, reason = AIAttackUtils.PlatoonGenerateSafePathTo(
            aiBrain, layer, spawn, drop, 200
        )

        if ScenarioInfo.Debug then
            LOG("Generator.Drops: " .. repr(path))
        end

        -- give the transports their commands
        for k, point in path do 
            IssueMove(transports, point)
        end

        IssueTransportUnload(transports, drop)
        IssueMove(transports, spawn)
    
        -- tell the units to attack once dropped
        ForkThread(
            OnDetachedThread,
            transportees, 
            function (units) 
                IssueFormMove(units, attack, 'GrowthFormation', 0)
            end 
        )
    end

    -- go here after break

end