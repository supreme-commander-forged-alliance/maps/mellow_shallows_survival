
local Areas = import("/maps/mellow_shallows_survival/utilities/Areas.lua")
local GenCommon = import("/maps/mellow_shallows_survival/generators/GenCommon.lua")

local pointCount = 4 

function Generate(count)

    for k = 1, count do 

        -- get a random scout area
        local scoutAreas = Areas.GetScoutAreas()
        local scoutArea = Areas.PickRandomArea(scoutAreas)

        -- get a bunch of random points in that area
        local scoutPoints = { }
        for l = 1, 4 do 
            table.insert(scoutPoints, Areas.PickRandomPointInArea(scoutArea))
        end

        -- determine a spawn location
        local spawnAreas = Areas.GetSpawnAirAreas()
        local spawnArea = Areas.PickRandomArea(spawnAreas)
        local spawnPoint = Areas.PickRandomPointInArea(spawnArea)

        -- spawn a random scout and send it on its way
        local bps = GenCommon.RetrieveBlueprints("ArmyOfGary", "Scouts")
        local unit = GenCommon.SpawnUnit(bps, "ArmyOfGary", spawnPoint)

        -- make the scout patrol those sacred points
        for k, point in scoutPoints do 
            IssuePatrol({unit}, point)
        end
    end

end