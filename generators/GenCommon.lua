
local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua')
local ScenarioFramework = import('/lua/ScenarioFramework.lua')

--- Periodically checks whether the units are still attached,
-- once detached runs the callback on the units.
function OnDetached(units, callback)
    ForkThread(OnDetachedThread, units, callback)
end

--- Periodically checks whether the units are still attached,
-- once detached runs the callback on the units. Assumes
-- this runs in a thread.
function OnDetachedThread(units, callback)
    -- keep running until no longer attached
    local allDead = false
    local attached = true

    while attached and (not allDead) do

        -- state to proof wrong
        allDead = true
        attached = false

        -- go over all units
        for k, unit in units do
            if not unit.Dead then

                -- we found one that is not dead
                allDead = false

                if unit:IsUnitState('Attached') then
                    -- we found one that is still attached
                    attached = true
                    break
                end
            end                
        end

        WaitSeconds(1)
    end

    -- do a callback if any survived the trip
    if not allDead then 
        callback(units)
    end
end

--- Retrieves the blueprint id from the given unit.
local function UnitToBlueprintID(unit)
    return unit:GetBlueprint().BlueprintId
end

--- Retrieves the blueprint ids from the given units.
local function UnitsToBlueprintID(units)
    local ids = { }
    for k, unit in units do 
        table.insert(ids, UnitToBlueprintID(unit))
    end
    return ids
end

-- cached all our bps
local cache = { }

--- Retrieves the blueprint ids for the given group of the given army. Assumes the group exists.
function RetrieveBlueprints(army, name)

    -- see if we got it in our cache
    if cache[name] then 
        return cache[name]
    end

    -- spawn in the units, retrieve their blueprint ids
    local units = ScenarioUtils.CreateArmyGroup(army, name, false, false)
    local bps = UnitsToBlueprintID(units)

    -- take 'em all out (to dinner)
    for k, unit in units do 
        unit:Destroy()
    end

    -- cache and return them
    cache[name] = bps
    return bps
end

--- Retrieves a whole 'packed together' blueprint ids from various unit groups. As an 
-- example, if 'name' = 'scout-' then it will cache all groups called 'scout-1', 'scout-2', etc.
function RetrieveBlueprintsTable(army, name)

    -- used to check if the group exists
    local scenario = ScenarioInfo.Env.Scenario 
    local tblNode = scenario.Armies[army].Units

    -- populate the cache
    local cache = { }
    for k = 1, 10, 1 do 

        -- check if the group exists
        local tblName = name .. k 
        local group = ScenarioUtils.FindUnitGroup(tblName, tblNode)
        if group then 
            -- if it exists, add the blueprint ids to the cache
            local bps = RetrieveBlueprints(army, tblName)
            table.insert(cache, bps)
        end
    end

    return cache
end

--- Picks a random element from the table
local function PickRandomElement(t)
    local n = table.getn(t)
    local i = math.floor(Random() * n) + 1
    return t[i]
end

--- Spawns all units of the given blueprint table
function SpawnGroup(bps, army, position)
    local units = { }
    for k, bp in bps do 
        local unit = CreateUnitHPR(bp, army, position[1], position[2], position[3], 0, 0, 0)
        table.insert(units, unit)
    end

    return units
end

--- Spawns a single random unit of the given blueprint table
function SpawnUnit(bps, army, position)
    local bp = PickRandomElement(bps)
    local unit = CreateUnitHPR(bp, army, position[1], position[2], position[3], 0, 0, 0)
    return unit
end