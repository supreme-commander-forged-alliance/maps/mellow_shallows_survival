
local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua')
local ScenarioFramework = import('/lua/ScenarioFramework.lua')

local Areas = import("/maps/mellow_shallows_survival/utilities/Areas.lua")
local NavalSpawns = import("/maps/mellow_shallows_survival/utilities/NavalSpawns.lua")

function OnStart() 

    local areas = Areas.GetSpawnNavyAreas()
    local area = Areas.PickRandomArea(areas)
    local spawn = Areas.PickRandomPointInArea(area)
    local attack = VECTOR3(256, 20, 256)

    local army = "ArmyOfGary"
    local group = NavalSpawns.RandomArmyGroup()
    local units = NavalSpawns.SpawnArmyGroup(group, army, spawn)

    IssueMove(units, attack)

end