# Mellow Shallows Survival

A product of a series on scripting in Supreme Commander: Forged Alliance.

## Prerequisites

This is not a series about the fundamentals of programming. Among others, I assume that you know the following concepts:
 - General syntax of lua
 - What variables are
 - What types (of variables) are, such as:
 - What functions / methods are
 - What a table (in Lua) is
 - What control structures are
 - What debugging (in general) is

We'll discuss more advanced topics throughout the series. If you're not aware of these fundamental topics I highly encourage you to understand these topics before starting the series. You can find more information at:
 - https://www.lua.org/pil/contents.html
 - http://www.dcc.ufrj.br/~fabiom/lua/
 - https://www.tutorialspoint.com/lua/index.htm
 - http://tylerneylon.com/a/learn-lua/

And the general reference to all default Lua functionality:
 - https://www.lua.org/manual/5.4/

These guides do not all apply directly to Supreme Commander. As an example, we have very little to no IO interaction.

## Series

 - [Session 1: introduction to basic functionality](session-1.md)

## License

The stratum layers (/env/layers) are from www.textures.com. I am obligated to add this text:
_One or more textures bundled with this project have been created with images from Textures.com. These images may not be redistributed by default. Please visit www.textures.com for more information._

All other assets are licensed with [CC-BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/).