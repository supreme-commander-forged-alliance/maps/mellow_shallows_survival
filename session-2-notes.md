### About debugging in general - writing your own debug code and how to use the moholog.
 - Have a global debug toggle
 - Explain about the moholog and its filter options
 - Explain about switching armies in the cheat window

### About threads in the context of Supreme Commander.
 - Talk about threads - in a rather simplistic manner
 - Show a small example with two while loops
 - Show what happens when you wait outside a thread
 - Show what a common name is for code that is expected to run in a thread

### About army and unit callbacks and how they relate to threads.
 - Talk about missions, show how nice it is to have that workspace of ours to query
 - Talk about the various objectives that are available and how we will intent to use them
 - Talk about callbacks of objectives and units

### About generating safe paths.
 - What is a path anyway?
 - What are pathing markers?
 - How do we generate a path (astar)
 - Wait - that is already implemented?! -> AIAttackUtilities.lua

### About computing the closest (safe) drop / spawn points.
 - Explain idea: find the closest or safest drop / spawn point from a given spawn / drop point
 - Approach one: Find closest using Vdist2 / vdist2sq
 - Approach two: find the closest _safe_ spawn / drop point
 - - Add the spawn / drop markers to the pathing nodes
 - - Copy the astar code and add in our own conditions on stop / start
 - - use it all

### Refactoring
 - When to make a more general function
 - When to move a function into a broader scope
 - How to detect overthinking



